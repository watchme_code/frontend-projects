# Frontend Project Collection Repository

## Projects Overview

### 1. FormSubmit.org - Contact Form Submission Service

[FormSubmit.org](https://formsubmit.org/) is a powerful and user-friendly form backend, API, and email service designed for HTML forms. It enables direct form submission to your inbox without requiring backend code or SMTP setup. This service is registration-free, operates on a speedy cloud infrastructure, and manages its email hosting to process outgoing messages. The emails received from form submissions feature a custom, minimal design, emphasizing the service's name and slogan. The website design incorporates predominantly green colors, flat design elements, a parallax effect, a self-drawn logo, and specially crafted illustrations.

### 2. Worldwide Cheese | Online Store

The [Worldwide Cheese](https://worldwidecheese.reqres.net/) online store emerged from a personal interest in exploring various cheese types from around the globe. This project is a compact webpage for an online cheese shop offering an assortment of cheeses from different countries. It was developed without any libraries or frameworks, featuring a custom mobile grid, media queries for responsiveness, and pure JavaScript for animations and effects. The imagery used in this project, especially the product photos, is a highlight, contributing significantly to the overall user experience.

### 3. UrRun Fitness App

The [UrRun Fitness App](https://urrun.reqres.net/) continuing with the flat design theme, I designed the fitness tracker app page. It uses moving elements stylized as waves, as well as animated application serials that switch when interacting with a text description of the features of this software. Some of the illustrations I took from stock, and some I drew myself as part of a refresher on working with vector graphics in Adobe Illustrator. The animation of the main section is done on a pure CSS. The text is borrowed from the description of the space telescope.

### 4. Pink Company

The [Pink Company](https://pinkcompany.reqres.net/). I have always liked and are now attracted by the trendy flat design, with bright illustrations in the style of minimalism. In this project, I made a web page for a company that is focused both on productive interaction with customers and maintains good relations between employees. I named it The Pink Company. The illustrations have been taken from ready-made sources and have been tweaked to fit the chosen format. Switching between mobile and desktop versions of the interface occurs automatically.

### 5. ColorSite | Career Platform

The [ColorSite | Career Platformy](https://colorsite.reqres.net/). When I started to study development, I also used some educational materials from the Pluralsight service. Now it is not just a set of courses, but a large training platform for professionals. However, I like their old design with iridescent colors on a dark background. In this project, I tried to do something based on their UI. I used, among other things, services Flatuicolors to select colors and Uigradients to fill elements with a gradient. I created a custom CSS grid to implement device adaptability; the interface effects were written on pure JS.

### 6. Angela Photo

The [Angela Photo](https://angelaphoto.reqres.net/). On some site with a collection of unusual design elements, I saw the effect of the background, which is a continuation of the main picture. I wanted to make a similar background - I took a photo of the amused photographer Angela Martin, who presents her work and is surrounded by green plants that interfere with the background elements. I used pastel colors and pictures of beautiful people. You can see how this website will look on your phone screen if you visit it from your mobile device.

### 7. DriveLux Car Service

The [DriveLux Car Service](https://drivelux.reqres.net/). And this is one of my favorite sites. Especially valuable for me are the illustrations - cars, freedom, warm weather and a leather interior with an automatic transmission and a high-powered engine. Cars for rent are presented not by everyday models of the middle class, but by brands above the middle segment. In many sources, I have come across a mention of a large rental corporation called Avis. The text description is based on motives and inspiration from the services of this company. The mobile version of the page is implemented as usual. Please pay attention to the illustrations again :)

### 8. Above and Beyond

The [Above and Beyond](https://videobg.reqres.net/). I've always liked video backgrounds, BUT provided that the video is lightweight and used when appropriate, and not flashy and annoying ads. I decided to add a website with such a video to my collection. During the creation of this web page, I had the feeling that I wanted to go beyond the usual boundaries of reality - hence the name: Above and Beyond. The yellow accent contrasts with the dark elements of the interface. Videos are compressed and looped for better compatibility with mobile devices and slow network.

### 9. Industrial Equipment

The [Industrial Equipment](https://equipment.reqres.net/). When I first started developing, I used the WordPress system. At the same time, I found on sale templates for websites of various organizations that provide services. Recently I remembered this and started developing a page for a company that supplies industrial equipment for construction and engineering needs. I thought that heavy equipment might not necessarily be portrayed in the extreme conditions of a construction site - I selected such illustrations to make them look beautiful. Text descriptions are deliberately made in the form of short and succinct slogans - business people visit our site and they need to immediately present the essence of our service. More detailed information can be obtained by phone or in separate sections located outside the main page. The yellow range is associated with the coloring of such equipment.